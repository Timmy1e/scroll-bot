#!/usr/bin/env sh

set -e

dir=$(dirname "$0")
printf ":: Moving working directory to '%s'\n" "${dir}"
cd "${dir}"

# shellcheck disable=SC1083
branch=$(git rev-parse --symbolic-full-name --abbrev-ref @{upstream} | sed 's!/! !')
printf "==> Fetching '%s'\n" "${branch}"
# shellcheck disable=SC2086
git fetch ${branch}

printf "==> Checking GIT\n"
# shellcheck disable=SC1083
if [ "$(git rev-parse HEAD)" = "$(git rev-parse @{u})" ]; then
	printf "  -> Already up-to-date\n"
else
	printf "  -> Pulling using git\n"
	git pull
	printf "  -> Building using PIP3\n"
	pip3 install .
	printf "  -> Restart Scroll Bot service\n"
	systemctl daemon-reload
	systemctl restart scroll-bot.service
fi

printf "==> Done updating Scroll Bot!\n"
exit 0
