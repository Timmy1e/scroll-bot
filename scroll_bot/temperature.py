"""
Copyright (C) 2018  Tim van Leuverden

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
"""

import requests
from .drawable import Drawable


class Temperature(Drawable):
    def __init__(self, animator, location, unit):
        # Sanity check
        assert unit == 'C' or unit == 'F'

        self.animator = animator
        self.location = location
        self.unit = unit
        self.weather: any = None
        self.update()

    def update(self):
        # noinspection PyBroadException
        try:
            result = requests.get('https://wttr.in/%s' % self.location, params={'format': 'j1'})
            self.weather = result.json()
        except:
            pass

    def draw(self, now):
        if not self.weather:
            return

        temp = str(self.weather['current_condition'][0]['temp_%c' % self.unit])

        self.animator.pixel(12, 0, 0.75)
        self.animator.pixel(12, 1, 0.5)

        self.animator.char(14, 0, self.unit)

        self.animator.char(8, 0, temp[-1])
        if len(temp) > 1:
            self.animator.char(4, 0, temp[-2])
            if len(temp) > 2:
                self.animator.char(0, 0, temp[-3])
