"""
Copyright (C) 2018  Tim van Leuverden

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
"""

from .drawable import Drawable


class Activity(Drawable):
    def __init__(self, screen):
        self.screen = screen

    def draw(self, now):
        milli = int(round((now.microsecond / 1000000) * 16))
        self.screen.pixel(self.solve_wrapping(milli - 3), 6, 0.25, True)
        self.screen.pixel(self.solve_wrapping(milli - 2), 6, 0.50, True)
        self.screen.pixel(self.solve_wrapping(milli - 1), 6, 0.75, True)
        self.screen.pixel(milli, 6, 1, False)

    def solve_wrapping(self, x):
        if x < 0:
            return self.screen.width + x
        if x >= self.screen.width:
            return x - (self.screen.width - 1)
        return x
