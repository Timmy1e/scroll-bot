"""
Copyright (C) 2018  Tim van Leuverden

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
"""

from .drawable import Drawable


class Date(Drawable):
    def __init__(self, animator):
        self.animator = animator

    def draw(self, now):
        self.draw_month(now)
        self.draw_day(now)
        self.draw_dash()

    def draw_month(self, now):
        month = now.month

        month_str = str(month)
        if month < 10:
            month_str = "0" + month_str

        self.animator.char(0, 0, month_str[0], 1)
        self.animator.char(4, 0, month_str[1], 1)

    def draw_day(self, now):
        day = now.day

        day_str = str(day)
        if day < 10:
            day_str = "0" + day_str

        self.animator.char(10, 0, day_str[0], 1)
        self.animator.char(14, 0, day_str[1], 1)

    def draw_dash(self):
        self.animator.pixel(7, 2, 0.75)
        self.animator.pixel(8, 2, 1)
        self.animator.pixel(9, 2, 0.75)
