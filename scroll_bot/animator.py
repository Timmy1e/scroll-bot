"""
Copyright (C) 2018  Tim van Leuverden

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
"""

from datetime import datetime


class Animator(object):
    def __init__(self, screen):
        self.screen = screen
        self.offset_x = 0
        self.offset_y = 0
        self.offset_scroll = 3

    def scroll_right(self, left_draw, right_draw, activity_draw, fps=120):
        width = self.screen.width + self.offset_scroll
        positions = range(width)

        self._scroll(width, positions, left_draw, right_draw, activity_draw, fps)

        self.offset_x = 0

    def scroll_left(self, left_draw, right_draw, activity_draw, fps=120):
        width = self.screen.width + self.offset_scroll
        positions = reversed(range(width))

        self._scroll(width, positions, left_draw, right_draw, activity_draw, fps)

        self.offset_x = 0

    def _scroll(self, width, positions, left_draw, right_draw, activity_draw, fps):
        for position in positions:
            now = datetime.now()
            self.screen.flush()

            self.offset_x = -position
            left_draw(now)

            self.offset_x = width - position
            right_draw(now)

            activity_draw(now)
            self.screen.draw()

    def offset(self, x, y):
        return (
            x + self.offset_x,
            y + self.offset_y
        )

    def pixel(self, x, y, brightness=1, no_brightness_fix=False):
        offset = self.offset(x, y)
        self.screen.pixel(offset[0], offset[1], brightness, no_brightness_fix)

    def char(self, x, y, char, brightness=1, no_brightness_fix=False, font=None, monospace=False):
        offset = self.offset(x, y)
        self.screen.char(offset[0], offset[1], char, brightness, no_brightness_fix, font, monospace)
