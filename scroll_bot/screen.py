"""
Copyright (C) 2018  Tim van Leuverden

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
"""

import scrollphathd as sph


class Screen(object):
    width = 17
    height = 7
    default_font = None

    def __init__(self, font, brightness=1):
        self.default_font = font
        self.brightness_max = brightness
        self.brightness_base = brightness
        sph.rotate(180)
        sph.set_font(font)
        sph.clear()

    @staticmethod
    def draw():
        sph.show()

    @staticmethod
    def flush():
        sph.clear()

    def auto_brightness(self, now):
        hour = now.hour

        if hour < 6:
            self.brightness_base = 0
        elif hour < 12:
            self.brightness_base = self.brightness_max * ((hour - 6) / 7)
        elif hour > 18:
            hour *= -1
            hour += 24
            self.brightness_base = self.brightness_max * (hour / 7)
        else:
            self.brightness_base = self.brightness_max

    def relative_brightness(self, brightness, no_brightness_fix=False):
        brightness_rel = self.brightness_base * brightness

        if not no_brightness_fix:
            if 0.1 > brightness_rel > 0:
                brightness_rel = 0.1
            elif brightness_rel > 1:
                brightness_rel = 1

        return brightness_rel

    def pixel(self, x, y, brightness=1, no_brightness_fix=False):
        if 0 <= x < self.width and 0 <= y < self.height:
            sph.set_pixel(x, y, self.relative_brightness(brightness, no_brightness_fix))

    def char(self, x, y, char, brightness=1, no_brightness_fix=False, font=default_font, monospace=False):
        if 0 <= x < self.width and 0 <= y < self.height:
            sph.draw_char(x, y, char, font, self.relative_brightness(brightness, no_brightness_fix), monospace)
