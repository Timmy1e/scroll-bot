"""
Copyright (C) 2018  Tim van Leuverden

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
"""
import os
import time as the_time
from .temperature import Temperature
from .animator import Animator
from .activity import Activity
from .screen import Screen
from .time import Time
from .date import Date
from scrollphathd.fonts import font3x5
from datetime import datetime
from threading import Thread


# -=-=- Main -=-=- #
def main():
    # Read the environment for configurable variables
    unit = str(os.environ.get('x_unit', 'C'))  # Temperature unit 'C' or 'F'
    location = str(os.environ.get('x_location', ''))  # Location, see wttr.in location documentation
    brightness_max = float(os.environ.get('x_max_brightness', 1))  # Maximum pixel brightness, as it can be blinding

    # Print configurable variables
    print('Using variables: unit = %c; location = "%s"; max brightness = %f' % (unit, location, brightness_max))

    # Create the runtime variables
    screen = Screen(font3x5, brightness_max)
    animator = Animator(screen)
    activity = Activity(screen)
    temperature = Temperature(animator, location, unit)
    time = Time(animator)
    date = Date(animator)

    # Left = temperature, middle = time, right = date
    scroll_location = 'middle'

    # Main runtime loop
    while True:
        # Get the current time/date
        now = datetime.now()
        # Empty the screen
        screen.flush()
        # Calculate the brightness
        screen.auto_brightness(now)

        # No need to update the screen if the brightness is less then 0.1, you can't see anything at all
        if screen.brightness_base < 0.1:
            # Draw the empty screen
            screen.draw()
            # The brightness shouldn't change for at least one hour, so let's wait and free up some system time
            the_time.sleep(3600)
            # No need to run any display code, go to the next iteration
            continue

        # For 5 seconds, on the second quarter, show the date
        if 15 < now.second < 19:
            if scroll_location != 'right':
                animator.scroll_right(time.draw, date.draw, activity.draw)
                scroll_location = 'right'
            else:
                date.draw(now)

        # For 5 seconds, on the forth quarter, show the temperature
        elif 45 < now.second < 49:
            if scroll_location != 'left':
                Thread(target=temperature.update).start()
                animator.scroll_left(temperature.draw, time.draw, activity.draw)
                scroll_location = 'left'
            else:
                temperature.draw(now)

        # All other times show the time
        else:
            if scroll_location == 'right':
                animator.scroll_left(time.draw, date.draw, activity.draw)
                scroll_location = 'middle'
            elif scroll_location == 'left':
                animator.scroll_right(temperature.draw, time.draw, activity.draw)
                scroll_location = 'middle'
            else:
                time.draw(now)

        # Draw the activity indicator along the bottom of the screen
        activity.draw(now)

        # Update the screen
        screen.draw()
