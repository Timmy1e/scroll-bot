"""
Copyright (C) 2018  Tim van Leuverden

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
"""

from .drawable import Drawable


class Time(Drawable):
    def __init__(self, animator):
        self.animator = animator

    def draw(self, now):
        self.draw_hour(now)
        self.draw_minute(now)
        self.draw_second(now)

    def draw_hour(self, now):
        hour = now.hour

        if hour > 12:
            hour %= 12

        hour_str = str(hour)
        if hour < 10:
            hour_str = "0" + hour_str

        self.animator.char(0, 0, hour_str[0], 1)
        self.animator.char(4, 0, hour_str[1], 1)

    def draw_minute(self, now):
        minute = now.minute

        minute_str = str(minute)
        if minute < 10:
            minute_str = "0" + minute_str

        self.animator.char(10, 0, minute_str[0], 1)
        self.animator.char(14, 0, minute_str[1], 1)

    def draw_second(self, now):
        if now.second % 2 == 0:
            self.animator.pixel(8, 1, 1)
            self.animator.pixel(8, 3, 1)
        elif now.hour > 11:
            self.animator.pixel(8, 2, 0.5)
