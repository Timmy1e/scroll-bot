#!/usr/bin/env python3

from setuptools import setup, find_packages
from os import path

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='scroll-bot',
    version='1.1.5',
    description='Scroll Bot clock and more',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/Timmy1e/scroll-bot/',
    author='Tim van Leuverdem',
    author_email='TvanLeuverden@Gmail.com',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],

    keywords='pimoroni scroll-bot clock date time weather i2c',
    packages=find_packages(exclude=[
        'docs'
        'tests'
    ]),
    install_requires=[
        'requests',
        'scrollphathd'
    ],
    entry_points={
        'console_scripts': [
            'scrollbot=scroll_bot.main:main',
        ],
    },
    project_urls={
        'Bug Reports': 'https://gitlab.com/Timmy1e/scroll-bot/issues/',
        'Funding': 'https://paypal.me/Timmy1e/',
        'Say Thanks!': 'http://saythanks.io/to/timmy1e',
        'Source': 'https://gitlab.com/Timmy1e/scroll-bot/',
    },
)

